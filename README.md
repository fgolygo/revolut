## To create a standalone application
run: **mvn package** command
The final jar file will have **-jar-with-dependencies** suffix

## To run only tests
run: **mvn test**