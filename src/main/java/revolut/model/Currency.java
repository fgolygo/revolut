package revolut.model;

public enum Currency {
    USD,
    GBP,
    EUR
}
