package revolut.model;

import java.math.BigDecimal;

public class Transfer {
    private int accountFromId;
    private int accountToId;
    private BigDecimal amount;

    public int getAccountFromId() {
        return accountFromId;
    }

    public int getAccountToId() {
        return accountToId;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
