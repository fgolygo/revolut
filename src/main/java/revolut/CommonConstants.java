package revolut;

public class CommonConstants {
    public static final String ACC_ID_ERROR_MSG = "Account with id: %s doesn't exist";
    public static final String AMOUNT_ERROR_MSG = "Amount has to be greater than 0, was: %s";
    public static final String BALANCE_ERROR_MSG = "Not enough funds";
}
