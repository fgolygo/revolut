package revolut;

import org.javatuples.Pair;
import revolut.model.Account;
import revolut.model.Currency;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static revolut.model.Currency.EUR;
import static revolut.model.Currency.GBP;
import static revolut.model.Currency.USD;

public class CurrencyExchanger {
    private static Map<Pair<Currency, Currency>, BigDecimal> CURRENCY_RATES = new HashMap<>();

    static {
        CURRENCY_RATES.put(new Pair<>(USD, GBP), BigDecimal.valueOf(0.77));
        CURRENCY_RATES.put(new Pair<>(USD, EUR), BigDecimal.valueOf(0.86));

        CURRENCY_RATES.put(new Pair<>(GBP, EUR), BigDecimal.valueOf(1.11));
        CURRENCY_RATES.put(new Pair<>(GBP, USD), BigDecimal.valueOf(1.29));

        CURRENCY_RATES.put(new Pair<>(EUR, USD), BigDecimal.valueOf(1.15));
        CURRENCY_RATES.put(new Pair<>(EUR, GBP), BigDecimal.valueOf(0.89));
    }

    public BigDecimal exchange(Account accFrom, Account accTo, BigDecimal amount) {
        Currency currFrom = accFrom.getCurrency();
        Currency currTo = accTo.getCurrency();

        if (currFrom == currTo) {
            return amount;
        }

        return amount.multiply(CURRENCY_RATES.get(new Pair<>(currFrom, currTo)));
    }
}
