package revolut;

import static revolut.CommonConstants.BALANCE_ERROR_MSG;

public class InsufficientBalanceException extends Exception {

    public InsufficientBalanceException() {
        super(BALANCE_ERROR_MSG);
    }
}
