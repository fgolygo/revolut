package revolut;

public class AccountRepositoryFactory {
    public static AccountRepository testAccountRepository = new AccountRepository();
    private static AccountRepository prodAccountRepository = new AccountRepository();

    public static AccountRepository get() {
        String profile = System.getProperty("profile", "PROD");

        if (profile.equals("PROD")) {
            return prodAccountRepository;
        } else if (profile.equals("TEST")) {
            return testAccountRepository;
        } else {
            throw new RuntimeException("Profile env variable has to be PROD or TEST");
        }
    }
}
