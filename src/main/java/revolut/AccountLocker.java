package revolut;

import revolut.model.Transfer;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

import static java.util.concurrent.TimeUnit.SECONDS;

public class AccountLocker {
    private static Map<Integer, ReentrantLock> ACCOUNT_LOCKS = new HashMap<>();
    private static Random RANDOM = new Random();

    public void lockTransferAccounts(Transfer transfer) {
        ReentrantLock lockFrom = ACCOUNT_LOCKS.get(transfer.getAccountFromId());
        ReentrantLock lockTo = ACCOUNT_LOCKS.get(transfer.getAccountToId());

        try {
            while (true) {
                if (lockFrom.tryLock(2, SECONDS) && lockTo.tryLock(2, SECONDS)) {
                    return;
                } else {
                    if (lockFrom.isHeldByCurrentThread()) {
                        lockFrom.unlock();
                    }

                    Thread.sleep(RANDOM.nextInt(2000));
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void unlockTransferAccounts(Transfer transfer) {
        ACCOUNT_LOCKS.get(transfer.getAccountFromId()).unlock();
        ACCOUNT_LOCKS.get(transfer.getAccountToId()).unlock();
    }

    void addNewAccountLock(int accId) {
        ACCOUNT_LOCKS.put(accId, new ReentrantLock());
    }
}
