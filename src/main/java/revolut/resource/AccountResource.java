package revolut.resource;

import revolut.model.Account;
import revolut.service.AccountService;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.util.Collection;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("accounts")
public class AccountResource {
    private AccountService accountService = new AccountService();

    @GET
    @Produces(APPLICATION_JSON)
    public Collection<Account> getAccounts() {
        return accountService.getAccounts();
    }

    @Path("/{accId}")
    @GET
    @Produces(APPLICATION_JSON)
    public Response getAccount(@PathParam("accId") int accId) {
        return accountService.getAccount(accId);
    }
}
