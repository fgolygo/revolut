package revolut.resource;

import revolut.model.Transfer;
import revolut.service.TransferService;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("transfers")
public class TransferResource {
    private TransferService transferService = new TransferService();

    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    public Response transfer(Transfer transfer) {
        return transferService.transfer(transfer);
    }
}
