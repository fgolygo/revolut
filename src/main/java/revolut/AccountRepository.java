package revolut;

import revolut.model.Account;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class AccountRepository {
    public Map<Integer, Account> ACCOUNTS = new HashMap<>();
    private AccountLocker accountLocker = new AccountLocker();

    public void addAccount(Account account) {
        int id = account.getId();
        ACCOUNTS.put(id, account);
        accountLocker.addNewAccountLock(id);
    }

    public Optional<Account> getAccount(int id) {
        return Optional.ofNullable(ACCOUNTS.get(id));
    }

    public Collection<Account> getAll() {
        return ACCOUNTS.values();
    }
}
