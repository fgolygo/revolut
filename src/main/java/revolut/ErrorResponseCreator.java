package revolut;

import org.json.JSONObject;

import javax.ws.rs.core.Response;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;

public class ErrorResponseCreator {

    public static Response createWith(String message) {
        JSONObject json = new JSONObject();
        json.put("errorMsg", message);
        return Response.status(BAD_REQUEST).entity(json.toString()).build();
    }
}
