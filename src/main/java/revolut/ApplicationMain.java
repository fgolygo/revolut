package revolut;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;
import revolut.model.Account;

import java.math.BigDecimal;

import static revolut.model.Currency.*;

public class ApplicationMain {

    public static void main(String[] args) throws Exception {
        new ApplicationMain().start();
    }

    private void start() throws Exception {
        createAccounts();
        Server server = configureServer();

        server.start();
        server.join();
    }

    private void createAccounts() {
        AccountRepository accountRepository = AccountRepositoryFactory.get();

        Account acc1 = new Account(1, new BigDecimal("7300.40"), EUR);
        Account acc2 = new Account(2, new BigDecimal("3800.20"), USD);
        Account acc3 = new Account(3, new BigDecimal("50"), GBP);

        accountRepository.addAccount(acc1);
        accountRepository.addAccount(acc2);
        accountRepository.addAccount(acc3);
    }

    private Server configureServer() {
        ResourceConfig resourceConfig = new ResourceConfig();
        resourceConfig.packages("revolut");
        resourceConfig.register(JacksonFeature.class);
        ServletContainer servletContainer = new ServletContainer(resourceConfig);
        ServletHolder servletHolder = new ServletHolder(servletContainer);
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/revolut");
        context.addServlet(servletHolder, "/*");

        Server server = new Server(8081);
        server.setHandler(context);
        return server;
    }
}
