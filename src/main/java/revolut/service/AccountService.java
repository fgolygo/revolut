package revolut.service;

import revolut.AccountRepository;
import revolut.AccountRepositoryFactory;
import revolut.ErrorResponseCreator;
import revolut.model.Account;

import javax.ws.rs.core.Response;
import java.util.Collection;
import java.util.Optional;

import static revolut.CommonConstants.ACC_ID_ERROR_MSG;

public class AccountService {
    private AccountRepository accountRepository = AccountRepositoryFactory.get();

    public Collection<Account> getAccounts() {
        return accountRepository.getAll();
    }

    public Response getAccount(int accId) {
        Optional<Account> account = accountRepository.getAccount(accId);

        return account.isPresent()
                ? Response.ok().entity(account.get()).build()
                : ErrorResponseCreator.createWith(String.format(ACC_ID_ERROR_MSG, accId));
    }
}
