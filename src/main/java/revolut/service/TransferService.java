package revolut.service;

import revolut.*;
import revolut.model.Account;
import revolut.model.Transfer;
import revolut.validation.TransferValidationResult;
import revolut.validation.TransferValidator;

import javax.ws.rs.core.Response;
import java.math.BigDecimal;

public class TransferService {
    private TransferValidator transferValidator = new TransferValidator();
    private AccountLocker accountLocker = new AccountLocker();
    private AccountRepository accountRepository = AccountRepositoryFactory.get();
    private CurrencyExchanger currencyExchanger = new CurrencyExchanger();

    public Response transfer(Transfer transfer) {
        TransferValidationResult validationResult = transferValidator.validate(transfer);
        if (validationResult.hasErrors()) {
            return ErrorResponseCreator.createWith(validationResult.getErrorMessage());
        }

        accountLocker.lockTransferAccounts(transfer);

        try {
            doTransfer(transfer);
        } catch (InsufficientBalanceException e) {
            accountLocker.unlockTransferAccounts(transfer);
            return ErrorResponseCreator.createWith(e.getMessage());
        }

        accountLocker.unlockTransferAccounts(transfer);
        return Response.ok().build();
    }

    private void doTransfer(Transfer transfer) throws InsufficientBalanceException {
        Account accFrom = accountRepository.getAccount(transfer.getAccountFromId()).get();
        Account accTo = accountRepository.getAccount(transfer.getAccountToId()).get();
        BigDecimal amount = transfer.getAmount();

        accFrom.withdraw(amount);
        BigDecimal amountToDeposit = currencyExchanger.exchange(accFrom, accTo, amount);
        accTo.deposit(amountToDeposit);
    }
}