package revolut.validation;

import revolut.AccountRepository;
import revolut.AccountRepositoryFactory;
import revolut.model.Transfer;

import java.math.BigDecimal;

import static java.math.BigDecimal.ZERO;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static revolut.CommonConstants.ACC_ID_ERROR_MSG;
import static revolut.CommonConstants.AMOUNT_ERROR_MSG;

public class TransferValidator {
    private AccountRepository accountRepository = AccountRepositoryFactory.get();

    public TransferValidationResult validate(Transfer transfer) {
        int accountFromId = transfer.getAccountFromId();
        int accountToId = transfer.getAccountToId();
        BigDecimal amount = transfer.getAmount();

        if (!accountRepository.getAccount(accountFromId).isPresent()) {
            return new TransferValidationResult(true, String.format(ACC_ID_ERROR_MSG, accountFromId));
        } else if (!accountRepository.getAccount(accountToId).isPresent()) {
            return new TransferValidationResult(true, String.format(ACC_ID_ERROR_MSG, accountToId));
        } else if (amount.compareTo(ZERO) <= 0) {
            return new TransferValidationResult(true, String.format(AMOUNT_ERROR_MSG, amount));
        }

        return new TransferValidationResult(false, EMPTY);
    }
}
