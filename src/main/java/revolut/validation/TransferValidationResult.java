package revolut.validation;

public class TransferValidationResult {
    private boolean hasErrors;
    private String errorMessage;

    public TransferValidationResult(boolean hasErrors, String errorMessage) {
        this.hasErrors = hasErrors;
        this.errorMessage = errorMessage;
    }

    public boolean hasErrors() {
        return hasErrors;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
