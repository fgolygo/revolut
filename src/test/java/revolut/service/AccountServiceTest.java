package revolut.service;

import io.restassured.RestAssured;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import revolut.AccountRepository;
import revolut.AccountRepositoryFactory;
import revolut.TestServer;
import revolut.model.Account;

import java.math.BigDecimal;

import static io.restassured.RestAssured.when;
import static java.math.BigDecimal.TEN;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import static revolut.CommonConstants.ACC_ID_ERROR_MSG;
import static revolut.model.Currency.EUR;
import static revolut.model.Currency.GBP;

public class AccountServiceTest {
    private AccountRepository accountRepository = AccountRepositoryFactory.get();

    @BeforeAll
    public static void setUp() {
        RestAssured.basePath = "/test";
    }

    @BeforeEach
    public void startServer() throws Exception {
        TestServer.start();
    }

    @AfterEach
    public void serverStop() throws Exception {
        TestServer.stop();
        AccountRepositoryFactory.testAccountRepository = new AccountRepository();
    }

    @Test
    public void shouldReturnAllAccounts() {
        Account acc1 = new Account(1, TEN, EUR);
        Account acc2 = new Account(2, TEN, GBP);
        accountRepository.addAccount(acc1);
        accountRepository.addAccount(acc2);

        when().get("accounts").then()
                .statusCode(200)
                .body("id", hasItems(1, 2));
    }

    @Test
    public void shouldReturnAccountWhenId1() {
        int accId = 1;
        BigDecimal balance = BigDecimal.valueOf(50d);
        Account acc1 = new Account(accId, balance, EUR);
        accountRepository.addAccount(acc1);

        when().get("accounts/1").then()
                .statusCode(200)
                .body("id", equalTo(accId))
                .body("balance", equalTo(balance.toString()))
                .body("currency", equalTo(EUR.name()));
    }

    @Test
    public void shouldReturnErrorMsgWhenAccountDoesNotExist() {
        when().get("accounts/1").then()
                .statusCode(400)
                .body("errorMsg", equalTo(String.format(ACC_ID_ERROR_MSG, 1)));
    }
}
