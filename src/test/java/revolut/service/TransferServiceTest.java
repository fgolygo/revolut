package revolut.service;

import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import revolut.AccountRepository;
import revolut.AccountRepositoryFactory;
import revolut.CurrencyExchanger;
import revolut.TestServer;
import revolut.model.Account;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static java.math.BigDecimal.TEN;
import static java.math.BigDecimal.ZERO;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static revolut.CommonConstants.*;
import static revolut.model.Currency.EUR;
import static revolut.model.Currency.GBP;

public class TransferServiceTest {
    private AccountRepository accountRepository = AccountRepositoryFactory.get();

    @BeforeAll
    public static void setUp() {
        RestAssured.basePath = "/test";
    }

    @BeforeEach
    public void startServer() throws Exception {
        TestServer.start();
    }

    @AfterEach
    public void serverStop() throws Exception {
        TestServer.stop();
        AccountRepositoryFactory.testAccountRepository = new AccountRepository();
    }

    @Test
    public void shouldReturnErrorMsgWhenAccountDoesntExist() {
        createTransferRequestWith("100").post("transfers").then()
                .statusCode(400)
                .body("errorMsg", equalTo(String.format(ACC_ID_ERROR_MSG, 1)));
    }

    @Test
    public void shouldReturnErrorMsgWhenAmountIsNegative() {
        Account acc1 = new Account(1, TEN, EUR);
        Account acc2 = new Account(2, TEN, EUR);
        accountRepository.addAccount(acc1);
        accountRepository.addAccount(acc2);

        createTransferRequestWith("-2")
                .post("transfers").then()
                .statusCode(400)
                .body("errorMsg", equalTo(String.format(AMOUNT_ERROR_MSG, -2)));
    }

    @Test
    public void shouldReturnErrorMsgWhenAmountIsZero() {
        Account acc1 = new Account(1, TEN, EUR);
        Account acc2 = new Account(2, TEN, EUR);
        accountRepository.addAccount(acc1);
        accountRepository.addAccount(acc2);

        createTransferRequestWith("0")
                .post("transfers").then()
                .statusCode(400)
                .body("errorMsg", equalTo(String.format(AMOUNT_ERROR_MSG, 0)));
    }

    @Test
    public void shouldNotModifyBalanceWhenInsuffiecentException() {
        Account acc1 = new Account(1, ZERO, EUR);
        Account acc2 = new Account(2, TEN, EUR);
        accountRepository.addAccount(acc1);
        accountRepository.addAccount(acc2);

        createTransferRequestWith("100")
                .post("transfers").then()
                .statusCode(400)
                .body("errorMsg", equalTo(BALANCE_ERROR_MSG));

        assertEquals(acc1.getBalance(), ZERO);
        assertEquals(acc2.getBalance(), TEN);
    }

    @Test
    public void shouldTransfer100FromAccId1ToAccId2WithNoMoneyConversion() {
        Account acc1 = new Account(1, TEN, EUR);
        Account acc2 = new Account(2, ZERO, EUR);
        accountRepository.addAccount(acc1);
        accountRepository.addAccount(acc2);

        createTransferRequestWith("10")
                .post("transfers").then()
                .statusCode(200);

        assertEquals(acc1.getBalance(), ZERO);
        assertEquals(acc2.getBalance(), TEN);
    }

    @Test
    public void shouldTransfer100FromAccId1ToAccId2WithMoneyConversion() {
        CurrencyExchanger currencyExchanger = new CurrencyExchanger();
        Account acc1 = new Account(1, TEN, EUR);
        Account acc2 = new Account(2, ZERO, GBP);
        accountRepository.addAccount(acc1);
        accountRepository.addAccount(acc2);
        BigDecimal expectedConvertedBalance = currencyExchanger.exchange(acc1, acc2, acc1.getBalance());

        createTransferRequestWith("10")
                .post("transfers").then()
                .statusCode(200);

        assertEquals(acc1.getBalance(), ZERO);
        assertEquals(acc2.getBalance(), expectedConvertedBalance);
    }

    private RequestSpecification createTransferRequestWith(String amount) {
        Map<String, String> account = new HashMap<>();
        account.put("accountFromId", "1");
        account.put("accountToId", "2");
        account.put("amount", amount);

        return given()
                .contentType("application/json")
                .body(account);
    }
}
